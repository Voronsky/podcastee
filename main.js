const {app, BrowserWindow} = require('electron');

let mainWindow;

/**
 * 
 */
function createWindow(){
    //Creates the browser window
    mainWindow = new BrowserWindow({width: 800, height: 600});


    //load the main window file
    mainWindow.loadFile('index.html');


    //When we close the window, we want to dereference the Mainwindow
    mainWindow.on('closed',()=>{
        mainWindow = null;
    });
}


//Creates the window when electron has finished loading everything
app.on('ready',createWindow);

app.on('window-all-closed',()=>{
    //This is part of electron-quick-start tut, but it is to make sure
    // That the app quits unless its darwin Mac OSX, then the user has
    // to force it to quit
    if(process.platform !== 'darwin'){
        app.quit(); 
    }
});

app.on('activate',()=>{
    if(mainWindow === null){
        createWindow();
    }
});
